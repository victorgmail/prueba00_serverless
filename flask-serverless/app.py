from flask import Flask, jsonify, make_response, render_template, redirect, url_for, request
from articulos import articulosFile
from pedidos import pedidosFile
from estadoPedidos import estadoPedidosFile
from database import mysql


app = Flask(__name__)

#configuracion de MySql

app.config['MYSQL_DATABASE_HOST'] = '10.10.1.41'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'JtBbS3ui*uXH'
app.config['MYSQL_DATABASE_DB'] = 'practica1'
mysql.init_app(app)

app.register_blueprint(articulosFile)
app.register_blueprint(pedidosFile)
app.register_blueprint(estadoPedidosFile)

@app.route("/")
def hello_from_root():
    return render_template('home.html')

@app.errorhandler(404)
def resource_not_found(e):
    return make_response(jsonify(error='Not found!'), 404)
    


