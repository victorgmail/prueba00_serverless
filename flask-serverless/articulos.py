from flask import Blueprint, request, render_template, url_for,  redirect
from database import mysql

articulosFile=Blueprint('articulosFile', __name__)


@articulosFile.route("/articulos", methods=["GET", "POST"])
@articulosFile.route("/articulos/<int:articulo_id_edit>", methods=["GET", "POST"])
def articulosFunction(articulo_id_edit=None):
    mensaje_flash = "no"
    connect = mysql.connect()
    cursor= connect.cursor()
    articulo_edit=None
    articulo_edit_name=None
    articulo_edit_value=None
    articulo_edit_id=None

    """el siguiente metodo condicionado es para ubicar el contenido actual de un campo
    en los labels de edicion solamente cuando exista una id de edicion"""
    if articulo_id_edit!=None:          
        cursor.execute(f"SELECT * FROM articulos WHERE id = {articulo_id_edit}")
        articulo_edit = cursor.fetchall()
        if request.method=='POST':
            producto = request.form['producto']
            valor = request.form['valor']
        articulo_edit_id=int(articulo_edit[0][0])
        articulo_edit_name=articulo_edit[0][1]
        articulo_edit_value=articulo_edit[0][2]
    else:
        if request.method=='POST':
            confirmar=request.form['confirmar']
            if confirmar=="save":
                producto = request.form['producto']
                valor = request.form['valor']
                if producto!="" and valor!="":
                    cursor.execute("INSERT INTO articulos VALUES(NULL, %s, %s)",(producto, valor))
                    cursor.connection.commit()
                    mensaje_flash = "cargado"
                elif producto!="" and valor=="":
                    mensaje_flash="error_valor"
                elif producto=="" and valor!="":
                    mensaje_flash="error_producto"
                else:
                    mensaje_flash= "error"

            elif confirmar=="edit":
                producto = request.form['producto']
                valor = request.form['valor']
                id_art = int(request.form['id'])
                if producto!="" and valor!="":
                    cursor.execute("UPDATE articulos SET producto = %s,valor = %s WHERE id = %s",(producto, valor, id_art))
                    cursor.connection.commit()
                elif producto!="" and valor=="":
                    mensaje_flash="error_valor"
                elif producto=="" and valor!="":
                    mensaje_flash="error_producto"
                else:
                    mensaje_flash= "error"

    cursor.execute("SELECT * FROM articulos")
    articulos = cursor.fetchall()
    cursor.close()
    return render_template('articulos.html', 
                            mensaje_flash=mensaje_flash,
                            articulos=articulos,
                            articulo_edit_name=articulo_edit_name,
                            articulo_edit_value=articulo_edit_value,
                            articulo_edit_id=articulo_edit_id,
                            articulo_id_edit=articulo_id_edit
                            )
    
@articulosFile.route('/borrar-articulo/<int:articulo_id>')
def articulo_borrar(articulo_id):
    connect = mysql.connect()
    cursor= connect.cursor()
    cursor.execute(f"DELETE FROM articulos WHERE id = {articulo_id}")
    cursor.connection.commit()
    return redirect(url_for('articulosFile.articulosFunction'))

@articulosFile.route('/editar-articulo/<int:articulo_id>', methods=["GET","POST"])
def articulo_editar(articulo_id):
    connect = mysql.connect()
    cursor= connect.cursor()
    cursor.execute("SELECT id FROM articulos WHERE id = %s", (articulo_id))
    articulo=cursor.fetchall()
    cursor.close()
    return redirect(url_for('articulosFile.articulosFunction',articulo_id_edit=articulo[0][0]))